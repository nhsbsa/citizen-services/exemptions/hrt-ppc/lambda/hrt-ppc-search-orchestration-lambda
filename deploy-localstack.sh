#!/bin/bash

npm run build
echo "copying node_modules to /dist folder"
cp -R node_modules dist
echo "zipping /dist folder"
zip -x "**/codecov" -9 -r -q hrt-ppc-search-orchestration-lambda.zip dist
echo "copying zip to /hrt-ppc-localstack folder"
cp hrt-ppc-search-orchestration-lambda.zip ../hrt-ppc-localstack/lambda-archives
echo "removing zip"
rm -f hrt-ppc-search-orchestration-lambda.zip
echo "removing node_modules from /dist folder"
rm -rf dist/node_modules