import { AxiosRequestConfig } from "axios";
import { loggerWithContext } from "@nhsbsa/hrt-ppc-npm-logging";
import { CitizenApi } from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { sliceArrayIntoChunks } from "./chunk-request";
import { Citizen, CitizenDao } from "./interfaces";

/**
 * The following class allows the retrieval of citizens, calls can be made to the first page
 * and sub-sequent pages if needed.
 */
export class GetCitizens {
  citizenApi: CitizenApi;
  constructor() {
    this.citizenApi = new CitizenApi();
  }

  public async makeRequestFirstPage(params, headers) {
    loggerWithContext().info(`Get citizens page [0]`);
    const citizenDao: CitizenDao = (await this.citizenApi.makeRequest({
      method: "GET",
      url: "/v1/citizens?" + this.toUrlString(params),
      headers,
      responseType: "json",
    })) || { _embedded: { citizens: [] } };

    loggerWithContext().info(
      `found [${citizenDao._embedded.citizens.length}] number of citizen records from page [0]`,
    );

    return citizenDao;
  }

  public async makeRequestsAdditionalPages(
    params,
    headers,
    totalPages: number,
  ) {
    const citizensResults: Citizen[] = [];
    const configArray: AxiosRequestConfig[] = [];
    for (let i = 1; i < totalPages; i++) {
      const newParams = { ...params }; // clone the params to avoid issues with other async calls
      newParams.page = i;
      loggerWithContext().info(`Get citizens page [${i}]`);
      const config: AxiosRequestConfig = {
        method: "GET",
        url: "/v1/citizens?" + this.toUrlString(newParams),
        headers,
        responseType: "json",
      };

      configArray.push(config);
    }

    const chunkedConfigArray: AxiosRequestConfig[][] = sliceArrayIntoChunks(
      configArray,
      20,
    );

    for (const batchConfig of chunkedConfigArray) {
      // promise.all allows you to make multiple axios requests at the same time.
      // It returns an array of the results of all your axios requests
      const promiseArray: Promise<CitizenDao>[] = [];
      for (const config of batchConfig) {
        promiseArray.push(this.citizenApi.makeRequest(config));
      }
      const resolvedPromises = await Promise.all(promiseArray);
      for (let i = 0; i < resolvedPromises.length; i++) {
        // This will give you access to the output of each API call
        const citizenDao: CitizenDao = resolvedPromises[i] || {
          _embedded: { citizens: [] },
        };

        citizensResults.push(...citizenDao._embedded.citizens);

        loggerWithContext().info(
          `found [${citizenDao._embedded.citizens.length}] number of citizen records from page [${citizenDao.page?.number}]`,
        );
      }
    }
    return citizensResults.flat();
  }

  private toUrlString(params): string {
    const requestParams = {
      ...(params.page && { page: params.page }),
      ...(params.size && { size: params.size }),
      ...(params.firstName && { firstName: params.firstName }),
      ...(params.lastName && { lastName: params.lastName }),
      ...(params.dateOfBirth && { dateOfBirth: params.dateOfBirth }),
      ...(params.postcode && { "addresses.postcode": params.postcode }),
    };
    return new URLSearchParams(requestParams).toString();
  }
}
