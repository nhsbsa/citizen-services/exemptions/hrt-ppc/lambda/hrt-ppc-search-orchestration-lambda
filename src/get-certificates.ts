import { AxiosRequestConfig } from "axios";
import { loggerWithContext } from "@nhsbsa/hrt-ppc-npm-logging";
import { CertificateApi } from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { sliceArrayIntoChunks } from "./chunk-request";
import { Certificate, CertificateDao } from "./interfaces";

/**
 * The following class allows the retrieval of certificates, calls can be made to the first page
 * and sub-sequent pages if needed.
 */
export class GetCertificates {
  certificateApi: CertificateApi;
  constructor() {
    this.certificateApi = new CertificateApi();
  }

  public async makeRequestFirstPage(params, headers) {
    loggerWithContext().info(`Get certificates page [0]`);
    const certificateDao: CertificateDao =
      (await this.certificateApi.makeRequest({
        method: "GET",
        url: "/v1/certificates/search/findByCitizens",
        params,
        headers,
        responseType: "json",
      })) || { _embedded: { certificates: [] } };

    loggerWithContext().info(
      `found [${certificateDao._embedded.certificates.length}] number of certificates records from page [0]`,
    );

    return certificateDao;
  }

  public async makeRequestsAdditionalPages(
    params,
    headers,
    totalPages: number,
  ) {
    const certificatesResults: Certificate[] = [];
    const configArray: AxiosRequestConfig[] = [];
    for (let i = 1; i < totalPages; i++) {
      const newParams = { ...params }; // clone the params to avoid issues with other async calls
      newParams.page = i;
      loggerWithContext().info(`Get certificates page [${i}]`);
      const config: AxiosRequestConfig = {
        method: "GET",
        url: "/v1/certificates/search/findByCitizens",
        params: newParams,
        headers,
        responseType: "json",
      };
      configArray.push(config);
    }

    const chunkedConfigArray: AxiosRequestConfig[][] = sliceArrayIntoChunks(
      configArray,
      20,
    );

    for (const batchConfig of chunkedConfigArray) {
      // promise.all allows you to make multiple axios requests at the same time.
      // It returns an array of the results of all your axios requests
      const promiseArray: Promise<CertificateDao>[] = [];
      for (const config of batchConfig) {
        promiseArray.push(this.certificateApi.makeRequest(config));
      }

      const resolvedPromises = await Promise.all(promiseArray);
      for (let i = 0; i < resolvedPromises.length; i++) {
        // This will give you access to the output of each API call
        const certificateDao: CertificateDao = resolvedPromises[i] || {
          _embedded: { certificates: [] },
        };

        certificatesResults.push(...certificateDao._embedded.certificates);

        loggerWithContext().info(
          `found [${certificateDao._embedded.certificates.length}] number of certificates records from page [${certificateDao.page?.number}]`,
        );
      }
    }
    return certificatesResults.flat();
  }
}
