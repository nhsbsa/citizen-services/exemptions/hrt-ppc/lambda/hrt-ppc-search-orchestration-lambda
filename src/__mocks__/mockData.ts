export const mockParams = {
  firstName: "John",
  lastName: "Smith",
  dateOfBirth: "1997-03-24",
  postcode: "NE15 8NY",
};

export const mockParamsWithCertificateFilters = {
  firstName: "John",
  lastName: "Smith",
  dateOfBirth: "1997-03-24",
  postcode: "NE15 8NY",
  endDate: "2022-03-24",
  startDate: "2023-03-24",
  reference: "ABCDE",
  status: "PENDING",
};

export const mockParamsWithPageOne = {
  firstName: "John",
  lastName: "Smith",
  dateOfBirth: "1997-03-24",
  postcode: "NE15 8NY",
  page: 1,
};

export const mockParamsWithPageTwo = {
  firstName: "John",
  lastName: "Smith",
  dateOfBirth: "1997-03-24",
  postcode: "NE15 8NY",
  page: 2,
};

export const mockHeaders = {
  "x-api-key": "x-api-key",
  channel: "ONLINE",
  "user-id": "ABCD",
  "correlation-id": "e7fffacb-4575-47ba-b998-e0169945cdd5",
};

export const mockOutgoingCallHeaders = {
  channel: "ONLINE",
  "user-id": "ABCD",
  "correlation-id": "e7fffacb-4575-47ba-b998-e0169945cdd5",
};
