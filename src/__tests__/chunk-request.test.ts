import { sliceArrayIntoChunks } from "../chunk-request";

test("[3, 4, 5, 6, 5] and trunk size is 3 should return [[3,4,5],[6,5]]", () => {
  expect(sliceArrayIntoChunks([3, 4, 5, 6, 5], 3)).toStrictEqual([
    [3, 4, 5],
    [6, 5],
  ]);
});

test("[3, 4, 5, 6, 5, 1] and trunk size is 2 should return [[3,4],[5,6],[5,1]]", () => {
  expect(sliceArrayIntoChunks([3, 4, 5, 6, 5, 1], 2)).toStrictEqual([
    [3, 4],
    [5, 6],
    [5, 1],
  ]);
});

test("[3, 4, 5, 6] and trunk size is 1 should return [[3],[4],[5],[6]]", () => {
  expect(sliceArrayIntoChunks([3, 4, 5, 6], 1)).toStrictEqual([
    [3],
    [4],
    [5],
    [6],
  ]);
});

test("[3, 4, 5, 6] and trunk size is 7 groups should return [[3],[4],[5],[6]]", () => {
  expect(sliceArrayIntoChunks([3, 4, 5, 6], 7)).toStrictEqual([[3, 4, 5, 6]]);
});

test("[3, 4, 5, 6] trunk to less than 1 number of chunk should throw error", () => {
  expect(() => {
    sliceArrayIntoChunks([3, 4, 5, 6], -1);
  }).toThrow(new Error("chunk size cannot less than 1"));
});
