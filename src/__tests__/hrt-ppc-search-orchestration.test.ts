import { searchOrchestration } from "../hrt-ppc-search-orchestration";
import {
  mockParams,
  mockParamsWithCertificateFilters,
  mockHeaders,
  mockOutgoingCallHeaders,
} from "../__mocks__/mockData";
import successResponseTwoCerts from "../__mocks__/single-page-response.json";
import successResponseTwoCertsWithSingleOther from "../__mocks__/single-page-response-with-single-other.json";
import successResponseMultiPageTwoCerts from "../__mocks__/multi-page-response-2-certificates.json";
import successResponseMultiPageTwoCertsWithMultiOthers from "../__mocks__/multi-page-response-2-certificates-13-other.json";
import successResponseMultiPageFiftyThreeCerts from "../__mocks__/multi-page-response-53-certificates.json";
import citizenApiResponseOnePage from "../__mocks__/api-responses/citizen-api/single-page-results.page-1.json";
import citizenApiResponseMultiPageFirst from "../__mocks__/api-responses/citizen-api/multi-page-results-page-1.json";
import citizenApiResponseMultiPageSecond from "../__mocks__/api-responses/citizen-api/multi-page-results-page-2.json";
import citizenApiResponseMultiPageThird from "../__mocks__/api-responses/citizen-api/multi-page-results-page-3.json";

import citizenApiResponseMultiPageFirst501Results from "../__mocks__/api-responses/citizen-api/501-result-pages/multi-page-results-page-1.json";
import citizenApiResponseMultiPageSecond501Results from "../__mocks__/api-responses/citizen-api/501-result-pages/multi-page-results-page-2.json";
import citizenApiResponseMultiPageThird501Results from "../__mocks__/api-responses/citizen-api/501-result-pages/multi-page-results-page-3.json";
import citizenApiResponseMultiPageFourth501Results from "../__mocks__/api-responses/citizen-api/501-result-pages/multi-page-results-page-4.json";
import citizenApiResponseMultiPageFith501Results from "../__mocks__/api-responses/citizen-api/501-result-pages/multi-page-results-page-5.json";
import citizenApiResponseMultiPageSixth501Results from "../__mocks__/api-responses/citizen-api/501-result-pages/multi-page-results-page-6.json";
import citizenApiResponseMultiPageSeventh501Results from "../__mocks__/api-responses/citizen-api/501-result-pages/multi-page-results-page-7.json";
import citizenApiResponseMultiPageEighth501Results from "../__mocks__/api-responses/citizen-api/501-result-pages/multi-page-results-page-8.json";
import citizenApiResponseMultiPageNinth501Results from "../__mocks__/api-responses/citizen-api/501-result-pages/multi-page-results-page-9.json";
import citizenApiResponseMultiPageTenth501Results from "../__mocks__/api-responses/citizen-api/501-result-pages/multi-page-results-page-10.json";
import citizenApiResponseMultiPageEleventh501Results from "../__mocks__/api-responses/citizen-api/501-result-pages/multi-page-results-page-11.json";

import certificateApiResponseOnePage from "../__mocks__/api-responses/certificate-api/single-page-results.page-1.json";
import certificateApiResponseMultiPageFirst from "../__mocks__/api-responses/certificate-api/multi-page-results-page-1.json";
import certificateApiResponseMultiPageSecond from "../__mocks__/api-responses/certificate-api/multi-page-results-page-2.json";
import certificateApiResponseMultiPageThird from "../__mocks__/api-responses/certificate-api/multi-page-results-page-3.json";
import otherCertificatesApiResponseSingle from "../__mocks__/expected-results/get-other-certificates/single-certificate-found.json";
import otherCertificatesApiResponseMultiple from "../__mocks__/expected-results/get-other-certificates/multiple-certificates-found.json";

import { GetCitizens } from "../get-citizens";
import { GetCertificates } from "../get-certificates";
import { GetOtherCertificates } from "../get-other-certificates";
import { CertificateDao, Citizen, CitizenDao } from "../interfaces";

let citizenSpyRequestFirstPage: jest.SpyInstance;
let citizenSpyRequestAdditionalPages: jest.SpyInstance;
let certificateSpyRequestFirstPage: jest.SpyInstance;
let certificateSpyRequestAdditionalPages: jest.SpyInstance;
let otherCertificatesSpy: jest.SpyInstance;

const PAGE_SIZE = 500;

beforeEach(() => {
  const firstPageMockReturnCert: CertificateDao = {
    _embedded: {
      certificates: certificateApiResponseOnePage._embedded.certificates,
    },
    page: certificateApiResponseOnePage.page,
  };
  certificateSpyRequestFirstPage = jest
    .spyOn(GetCertificates.prototype, "makeRequestFirstPage")
    .mockResolvedValue(firstPageMockReturnCert);
  certificateSpyRequestAdditionalPages = jest.spyOn(
    GetCertificates.prototype,
    "makeRequestsAdditionalPages",
  );
  otherCertificatesSpy = jest
    .spyOn(GetOtherCertificates.prototype, "makeRequestSearchByPersonalDetails")
    .mockResolvedValue({ certificates: [] });
});

afterEach(() => {
  jest.resetAllMocks();
  jest.restoreAllMocks();
});

describe("search orchestration", () => {
  it("single page response 2 citizens and first citizen has 2 certificates, no 'other' certificates", async () => {
    const firstPageMockReturn: CitizenDao = {
      _embedded: { citizens: citizenApiResponseOnePage._embedded.citizens },
      page: citizenApiResponseOnePage.page,
    };
    citizenSpyRequestFirstPage = jest
      .spyOn(GetCitizens.prototype, "makeRequestFirstPage")
      .mockResolvedValue(firstPageMockReturn);

    const result = await searchOrchestration({
      params: mockParams,
      headers: mockHeaders,
    });

    expect(otherCertificatesSpy).toBeCalledTimes(1);
    expect(otherCertificatesSpy).toBeCalledWith(mockParams);
    expect(citizenSpyRequestFirstPage).toBeCalledTimes(1);
    expect(citizenSpyRequestFirstPage).toHaveBeenCalledWith(
      mockParams,
      mockOutgoingCallHeaders,
    );
    expect(certificateSpyRequestFirstPage).toBeCalledTimes(1);
    expect(certificateSpyRequestFirstPage).toHaveBeenCalledWith(
      {
        citizenIds:
          "0a004b01-8515-1a6c-8185-1558ae970000,0a004b01-8515-1a6c-8185-1558e1e10003",
        size: PAGE_SIZE,
      },
      mockOutgoingCallHeaders,
    );
    expect(certificateSpyRequestAdditionalPages).toBeCalledTimes(1);
    expect(certificateSpyRequestAdditionalPages).toHaveBeenCalledWith(
      {
        citizenIds:
          "0a004b01-8515-1a6c-8185-1558ae970000,0a004b01-8515-1a6c-8185-1558e1e10003",
        size: PAGE_SIZE,
      },
      mockOutgoingCallHeaders,
      certificateApiResponseOnePage.page.totalPages,
    );
    expect(result).toStrictEqual(successResponseTwoCerts);
  });

  it("single page response 2 citizens and first citizen has 2 certificates, single 'other' certificate", async () => {
    const firstPageMockReturn: CitizenDao = {
      _embedded: { citizens: citizenApiResponseOnePage._embedded.citizens },
      page: citizenApiResponseOnePage.page,
    };
    citizenSpyRequestFirstPage = jest
      .spyOn(GetCitizens.prototype, "makeRequestFirstPage")
      .mockResolvedValue(firstPageMockReturn);
    otherCertificatesSpy = jest
      .spyOn(
        GetOtherCertificates.prototype,
        "makeRequestSearchByPersonalDetails",
      )
      .mockResolvedValue({ certificates: otherCertificatesApiResponseSingle });

    const result = await searchOrchestration({
      params: mockParams,
      headers: mockHeaders,
    });

    expect(otherCertificatesSpy).toBeCalledTimes(1);
    expect(otherCertificatesSpy).toBeCalledWith(mockParams);
    expect(citizenSpyRequestFirstPage).toBeCalledTimes(1);
    expect(citizenSpyRequestFirstPage).toHaveBeenCalledWith(
      mockParams,
      mockOutgoingCallHeaders,
    );
    expect(certificateSpyRequestFirstPage).toBeCalledTimes(1);
    expect(certificateSpyRequestFirstPage).toHaveBeenCalledWith(
      {
        citizenIds:
          "0a004b01-8515-1a6c-8185-1558ae970000,0a004b01-8515-1a6c-8185-1558e1e10003",
        size: PAGE_SIZE,
      },
      mockOutgoingCallHeaders,
    );
    expect(certificateSpyRequestAdditionalPages).toBeCalledTimes(1);
    expect(certificateSpyRequestAdditionalPages).toHaveBeenCalledWith(
      {
        citizenIds:
          "0a004b01-8515-1a6c-8185-1558ae970000,0a004b01-8515-1a6c-8185-1558e1e10003",
        size: PAGE_SIZE,
      },
      mockOutgoingCallHeaders,
      certificateApiResponseOnePage.page.totalPages,
    );
    expect(result).toStrictEqual(successResponseTwoCertsWithSingleOther);
  });

  it("multi-page success with 5 citizens and fourth citizen has 2 certificates, no 'other' certificates", async () => {
    const firstPageMockReturn: CitizenDao = {
      _embedded: {
        citizens: citizenApiResponseMultiPageFirst._embedded.citizens,
      },
      page: citizenApiResponseMultiPageFirst.page,
    };
    const secondPageMockReturn: CitizenDao = {
      _embedded: {
        citizens: citizenApiResponseMultiPageSecond._embedded.citizens,
      },
    };
    const thirdPageMockReturn: CitizenDao = {
      _embedded: {
        citizens: citizenApiResponseMultiPageThird._embedded.citizens,
      },
    };
    citizenSpyRequestFirstPage = jest
      .spyOn(GetCitizens.prototype, "makeRequestFirstPage")
      .mockResolvedValue(firstPageMockReturn);
    citizenSpyRequestAdditionalPages = jest
      .spyOn(GetCitizens.prototype, "makeRequestsAdditionalPages")
      .mockResolvedValue(
        secondPageMockReturn._embedded.citizens.concat(
          ...thirdPageMockReturn._embedded.citizens,
        ),
      );

    const result = await searchOrchestration({
      params: mockParams,
      headers: mockHeaders,
    });

    expect(otherCertificatesSpy).toBeCalledTimes(1);
    expect(otherCertificatesSpy).toBeCalledWith(mockParams);
    expect(citizenSpyRequestFirstPage).toBeCalledTimes(1);
    expect(citizenSpyRequestFirstPage).toHaveBeenCalledWith(
      mockParams,
      mockOutgoingCallHeaders,
    );
    expect(citizenSpyRequestAdditionalPages).toBeCalledTimes(1);
    expect(citizenSpyRequestAdditionalPages).toHaveBeenCalledWith(
      mockParams,
      mockOutgoingCallHeaders,
      firstPageMockReturn.page?.totalPages || 0,
    );
    expect(certificateSpyRequestFirstPage).toBeCalledTimes(1);
    expect(certificateSpyRequestFirstPage).toHaveBeenCalledWith(
      {
        citizenIds:
          "c0a8012f-852e-13ca-8185-356ab044009c,c0a8012f-852e-13ca-8185-356ab531009f,c0a8012f-852e-13ca-8185-356ab94d00a2,0a004b01-8515-1a6c-8185-1558ae970000,c0a8012f-852e-13ca-8185-356ab044009c",
        size: PAGE_SIZE,
      },
      mockOutgoingCallHeaders,
    );
    expect(certificateSpyRequestAdditionalPages).toBeCalledTimes(1);
    expect(certificateSpyRequestAdditionalPages).toHaveBeenCalledWith(
      {
        citizenIds:
          "c0a8012f-852e-13ca-8185-356ab044009c,c0a8012f-852e-13ca-8185-356ab531009f,c0a8012f-852e-13ca-8185-356ab94d00a2,0a004b01-8515-1a6c-8185-1558ae970000,c0a8012f-852e-13ca-8185-356ab044009c",
        size: PAGE_SIZE,
      },
      mockOutgoingCallHeaders,
      certificateApiResponseOnePage.page.totalPages,
    );
    expect(result).toStrictEqual(successResponseMultiPageTwoCerts);
  });

  it("multi-page success with 5 citizens and fourth citizen has 2 certificates, multiple other certificates", async () => {
    const firstPageMockReturn: CitizenDao = {
      _embedded: {
        citizens: citizenApiResponseMultiPageFirst._embedded.citizens,
      },
      page: citizenApiResponseMultiPageFirst.page,
    };
    const secondPageMockReturn: CitizenDao = {
      _embedded: {
        citizens: citizenApiResponseMultiPageSecond._embedded.citizens,
      },
    };
    const thirdPageMockReturn: CitizenDao = {
      _embedded: {
        citizens: citizenApiResponseMultiPageThird._embedded.citizens,
      },
    };
    citizenSpyRequestFirstPage = jest
      .spyOn(GetCitizens.prototype, "makeRequestFirstPage")
      .mockResolvedValue(firstPageMockReturn);
    citizenSpyRequestAdditionalPages = jest
      .spyOn(GetCitizens.prototype, "makeRequestsAdditionalPages")
      .mockResolvedValue(
        secondPageMockReturn._embedded.citizens.concat(
          ...thirdPageMockReturn._embedded.citizens,
        ),
      );
    otherCertificatesSpy = jest
      .spyOn(
        GetOtherCertificates.prototype,
        "makeRequestSearchByPersonalDetails",
      )
      .mockResolvedValue({
        certificates: otherCertificatesApiResponseMultiple,
      });

    const result = await searchOrchestration({
      params: mockParams,
      headers: mockHeaders,
    });

    expect(otherCertificatesSpy).toBeCalledTimes(1);
    expect(otherCertificatesSpy).toBeCalledWith(mockParams);
    expect(citizenSpyRequestFirstPage).toBeCalledTimes(1);
    expect(citizenSpyRequestFirstPage).toHaveBeenCalledWith(
      mockParams,
      mockOutgoingCallHeaders,
    );
    expect(citizenSpyRequestAdditionalPages).toBeCalledTimes(1);
    expect(citizenSpyRequestAdditionalPages).toHaveBeenCalledWith(
      mockParams,
      mockOutgoingCallHeaders,
      firstPageMockReturn.page?.totalPages || 0,
    );
    expect(certificateSpyRequestFirstPage).toBeCalledTimes(1);
    expect(certificateSpyRequestFirstPage).toHaveBeenCalledWith(
      {
        citizenIds:
          "c0a8012f-852e-13ca-8185-356ab044009c,c0a8012f-852e-13ca-8185-356ab531009f,c0a8012f-852e-13ca-8185-356ab94d00a2,0a004b01-8515-1a6c-8185-1558ae970000,c0a8012f-852e-13ca-8185-356ab044009c",
        size: PAGE_SIZE,
      },
      mockOutgoingCallHeaders,
    );
    expect(certificateSpyRequestAdditionalPages).toBeCalledTimes(1);
    expect(certificateSpyRequestAdditionalPages).toHaveBeenCalledWith(
      {
        citizenIds:
          "c0a8012f-852e-13ca-8185-356ab044009c,c0a8012f-852e-13ca-8185-356ab531009f,c0a8012f-852e-13ca-8185-356ab94d00a2,0a004b01-8515-1a6c-8185-1558ae970000,c0a8012f-852e-13ca-8185-356ab044009c",
        size: PAGE_SIZE,
      },
      mockOutgoingCallHeaders,
      certificateApiResponseOnePage.page.totalPages,
    );
    expect(result).toStrictEqual(
      successResponseMultiPageTwoCertsWithMultiOthers,
    );
  });

  it("multi-page success with 5 citizens, first has 52 certificates, third has 1 certificate, no 'other' certificates", async () => {
    const firstPageMockReturnCitizen: CitizenDao = {
      _embedded: {
        citizens: citizenApiResponseMultiPageFirst._embedded.citizens,
      },
      page: citizenApiResponseMultiPageFirst.page,
    };
    const secondPageMockReturnCitizen: CitizenDao = {
      _embedded: {
        citizens: citizenApiResponseMultiPageSecond._embedded.citizens,
      },
    };
    const thirdPageMockReturnCitizen: CitizenDao = {
      _embedded: {
        citizens: citizenApiResponseMultiPageThird._embedded.citizens,
      },
    };
    citizenSpyRequestFirstPage = jest
      .spyOn(GetCitizens.prototype, "makeRequestFirstPage")
      .mockResolvedValue(firstPageMockReturnCitizen);
    citizenSpyRequestAdditionalPages = jest
      .spyOn(GetCitizens.prototype, "makeRequestsAdditionalPages")
      .mockResolvedValue(
        secondPageMockReturnCitizen._embedded.citizens.concat(
          thirdPageMockReturnCitizen._embedded.citizens,
        ),
      );

    const firstPageMockReturnCert: CertificateDao = {
      _embedded: {
        certificates:
          certificateApiResponseMultiPageFirst._embedded.certificates,
      },
      page: certificateApiResponseMultiPageFirst.page,
    };
    const secondPageMockReturnCert: CertificateDao = {
      _embedded: {
        certificates:
          certificateApiResponseMultiPageSecond._embedded.certificates,
      },
      page: certificateApiResponseMultiPageSecond.page,
    };
    const thirdPageMockReturnCert: CertificateDao = {
      _embedded: {
        certificates:
          certificateApiResponseMultiPageThird._embedded.certificates,
      },
      page: certificateApiResponseMultiPageThird.page,
    };
    certificateSpyRequestFirstPage = jest
      .spyOn(GetCertificates.prototype, "makeRequestFirstPage")
      .mockResolvedValue(firstPageMockReturnCert);
    certificateSpyRequestAdditionalPages = jest
      .spyOn(GetCertificates.prototype, "makeRequestsAdditionalPages")
      .mockResolvedValue(
        secondPageMockReturnCert._embedded.certificates.concat(
          thirdPageMockReturnCert._embedded.certificates,
        ),
      );

    const result = await searchOrchestration({
      params: mockParams,
      headers: mockHeaders,
    });

    expect(otherCertificatesSpy).toBeCalledTimes(1);
    expect(otherCertificatesSpy).toBeCalledWith(mockParams);
    expect(citizenSpyRequestFirstPage).toBeCalledTimes(1);
    expect(citizenSpyRequestFirstPage).toHaveBeenCalledWith(
      mockParams,
      mockOutgoingCallHeaders,
    );
    expect(citizenSpyRequestAdditionalPages).toBeCalledTimes(1);
    expect(citizenSpyRequestAdditionalPages).toHaveBeenCalledWith(
      mockParams,
      mockOutgoingCallHeaders,
      firstPageMockReturnCitizen.page?.totalPages || 0,
    );
    expect(certificateSpyRequestFirstPage).toBeCalledTimes(1);
    expect(certificateSpyRequestFirstPage).toHaveBeenCalledWith(
      {
        citizenIds:
          "c0a8012f-852e-13ca-8185-356ab044009c,c0a8012f-852e-13ca-8185-356ab531009f,c0a8012f-852e-13ca-8185-356ab94d00a2,0a004b01-8515-1a6c-8185-1558ae970000,c0a8012f-852e-13ca-8185-356ab044009c",
        size: PAGE_SIZE,
      },
      mockOutgoingCallHeaders,
    );
    expect(certificateSpyRequestAdditionalPages).toBeCalledTimes(1);
    expect(certificateSpyRequestAdditionalPages).toHaveBeenCalledWith(
      {
        citizenIds:
          "c0a8012f-852e-13ca-8185-356ab044009c,c0a8012f-852e-13ca-8185-356ab531009f,c0a8012f-852e-13ca-8185-356ab94d00a2,0a004b01-8515-1a6c-8185-1558ae970000,c0a8012f-852e-13ca-8185-356ab044009c",
        size: PAGE_SIZE,
      },
      mockOutgoingCallHeaders,
      certificateApiResponseMultiPageFirst.page.totalPages,
    );
    expect(result).toStrictEqual(successResponseMultiPageFiftyThreeCerts);
  });

  it("multi-page success with 501 citizens, some have certificates, no other certificates", async () => {
    const firstPageMockReturnCitizen: CitizenDao = {
      _embedded: {
        citizens: citizenApiResponseMultiPageFirst501Results._embedded.citizens,
      },
      page: citizenApiResponseMultiPageFirst501Results.page,
    };
    const secondPageMockReturnCitizen: CitizenDao = {
      _embedded: {
        citizens:
          citizenApiResponseMultiPageSecond501Results._embedded.citizens,
      },
    };
    const thirdPageMockReturnCitizen: CitizenDao = {
      _embedded: {
        citizens: citizenApiResponseMultiPageThird501Results._embedded.citizens,
      },
    };
    const fourthPageMockReturnCitizen: CitizenDao = {
      _embedded: {
        citizens:
          citizenApiResponseMultiPageFourth501Results._embedded.citizens,
      },
    };
    const fithPageMockReturnCitizen: CitizenDao = {
      _embedded: {
        citizens: citizenApiResponseMultiPageFith501Results._embedded.citizens,
      },
    };
    const sixthPageMockReturnCitizen: CitizenDao = {
      _embedded: {
        citizens: citizenApiResponseMultiPageSixth501Results._embedded.citizens,
      },
    };
    const seventhPageMockReturnCitizen: CitizenDao = {
      _embedded: {
        citizens:
          citizenApiResponseMultiPageSeventh501Results._embedded.citizens,
      },
    };
    const eigthPageMockReturnCitizen: CitizenDao = {
      _embedded: {
        citizens:
          citizenApiResponseMultiPageEighth501Results._embedded.citizens,
      },
    };
    const ninthPageMockReturnCitizen: CitizenDao = {
      _embedded: {
        citizens: citizenApiResponseMultiPageNinth501Results._embedded.citizens,
      },
    };
    const tenthPageMockReturnCitizen: CitizenDao = {
      _embedded: {
        citizens: citizenApiResponseMultiPageTenth501Results._embedded.citizens,
      },
    };
    const eleventhPageMockReturnCitizen: CitizenDao = {
      _embedded: {
        citizens:
          citizenApiResponseMultiPageEleventh501Results._embedded.citizens,
      },
    };
    citizenSpyRequestFirstPage = jest
      .spyOn(GetCitizens.prototype, "makeRequestFirstPage")
      .mockResolvedValue(firstPageMockReturnCitizen);
    citizenSpyRequestAdditionalPages = jest
      .spyOn(GetCitizens.prototype, "makeRequestsAdditionalPages")
      .mockResolvedValue(
        secondPageMockReturnCitizen._embedded.citizens
          .concat(thirdPageMockReturnCitizen._embedded.citizens)
          .concat(fourthPageMockReturnCitizen._embedded.citizens)
          .concat(fithPageMockReturnCitizen._embedded.citizens)
          .concat(sixthPageMockReturnCitizen._embedded.citizens)
          .concat(seventhPageMockReturnCitizen._embedded.citizens)
          .concat(eigthPageMockReturnCitizen._embedded.citizens)
          .concat(ninthPageMockReturnCitizen._embedded.citizens)
          .concat(tenthPageMockReturnCitizen._embedded.citizens)
          .concat(eleventhPageMockReturnCitizen._embedded.citizens),
      );

    const firstPageMockReturnCert: CertificateDao = {
      _embedded: {
        certificates:
          certificateApiResponseMultiPageFirst._embedded.certificates,
      },
      page: certificateApiResponseMultiPageFirst.page,
    };
    const secondPageMockReturnCert: CertificateDao = {
      _embedded: {
        certificates:
          certificateApiResponseMultiPageSecond._embedded.certificates,
      },
      page: certificateApiResponseMultiPageSecond.page,
    };
    const thirdPageMockReturnCert: CertificateDao = {
      _embedded: {
        certificates:
          certificateApiResponseMultiPageThird._embedded.certificates,
      },
      page: certificateApiResponseMultiPageThird.page,
    };
    certificateSpyRequestFirstPage = jest
      .spyOn(GetCertificates.prototype, "makeRequestFirstPage")
      .mockResolvedValue(firstPageMockReturnCert);
    certificateSpyRequestAdditionalPages = jest
      .spyOn(GetCertificates.prototype, "makeRequestsAdditionalPages")
      .mockResolvedValue(
        secondPageMockReturnCert._embedded.certificates.concat(
          thirdPageMockReturnCert._embedded.certificates,
        ),
      );

    await searchOrchestration({
      params: mockParams,
      headers: mockHeaders,
    });

    const firstChunkCitizenIds = citizenIdsDelimited(
      firstPageMockReturnCitizen._embedded.citizens
        .concat(secondPageMockReturnCitizen._embedded.citizens)
        .concat(thirdPageMockReturnCitizen._embedded.citizens),
    );
    const secondChunkCitizenIds = citizenIdsDelimited(
      fourthPageMockReturnCitizen._embedded.citizens
        .concat(fithPageMockReturnCitizen._embedded.citizens)
        .concat(sixthPageMockReturnCitizen._embedded.citizens),
    );
    const thirdChunkCitizenIds = citizenIdsDelimited(
      seventhPageMockReturnCitizen._embedded.citizens
        .concat(eigthPageMockReturnCitizen._embedded.citizens)
        .concat(ninthPageMockReturnCitizen._embedded.citizens),
    );
    const fourthChunkCitizenIds = citizenIdsDelimited(
      tenthPageMockReturnCitizen._embedded.citizens.concat(
        eleventhPageMockReturnCitizen._embedded.citizens,
      ),
    );

    expect(otherCertificatesSpy).toBeCalledTimes(1);
    expect(otherCertificatesSpy).toBeCalledWith(mockParams);
    expect(citizenSpyRequestFirstPage).toBeCalledTimes(1);
    expect(citizenSpyRequestFirstPage).toHaveBeenCalledWith(
      mockParams,
      mockOutgoingCallHeaders,
    );
    expect(citizenSpyRequestAdditionalPages).toBeCalledTimes(1);
    expect(citizenSpyRequestAdditionalPages).toHaveBeenCalledWith(
      mockParams,
      mockOutgoingCallHeaders,
      firstPageMockReturnCitizen.page?.totalPages || 0,
    );
    expect(certificateSpyRequestFirstPage).toBeCalledTimes(4);
    expect(certificateSpyRequestFirstPage).toHaveBeenNthCalledWith(
      1,
      {
        citizenIds: firstChunkCitizenIds,
        size: PAGE_SIZE,
      },
      mockOutgoingCallHeaders,
    );
    expect(certificateSpyRequestFirstPage).toHaveBeenNthCalledWith(
      2,
      {
        citizenIds: secondChunkCitizenIds,
        size: PAGE_SIZE,
      },
      mockOutgoingCallHeaders,
    );
    expect(certificateSpyRequestFirstPage).toHaveBeenNthCalledWith(
      3,
      {
        citizenIds: thirdChunkCitizenIds,
        size: PAGE_SIZE,
      },
      mockOutgoingCallHeaders,
    );
    expect(certificateSpyRequestFirstPage).toHaveBeenNthCalledWith(
      4,
      {
        citizenIds: fourthChunkCitizenIds,
        size: PAGE_SIZE,
      },
      mockOutgoingCallHeaders,
    );
    expect(certificateSpyRequestAdditionalPages).toBeCalledTimes(4);
    expect(certificateSpyRequestAdditionalPages).toHaveBeenNthCalledWith(
      1,
      {
        citizenIds: firstChunkCitizenIds,
        size: PAGE_SIZE,
      },
      mockOutgoingCallHeaders,
      certificateApiResponseMultiPageFirst.page.totalPages,
    );
    expect(certificateSpyRequestAdditionalPages).toHaveBeenNthCalledWith(
      2,
      {
        citizenIds: secondChunkCitizenIds,
        size: PAGE_SIZE,
      },
      mockOutgoingCallHeaders,
      certificateApiResponseMultiPageFirst.page.totalPages,
    );
    expect(certificateSpyRequestAdditionalPages).toHaveBeenNthCalledWith(
      3,
      {
        citizenIds: thirdChunkCitizenIds,
        size: PAGE_SIZE,
      },
      mockOutgoingCallHeaders,
      certificateApiResponseMultiPageFirst.page.totalPages,
    );
    expect(certificateSpyRequestAdditionalPages).toHaveBeenNthCalledWith(
      4,
      {
        citizenIds: fourthChunkCitizenIds,
        size: PAGE_SIZE,
      },
      mockOutgoingCallHeaders,
      certificateApiResponseMultiPageFirst.page.totalPages,
    );
  });

  it("should respond an empty hrt-ppcs list when no citizens found, no 'other' certificates", async () => {
    const firstPageMockReturn: CitizenDao = {
      _embedded: { citizens: [] },
      page: citizenApiResponseOnePage.page,
    };
    citizenSpyRequestFirstPage = jest
      .spyOn(GetCitizens.prototype, "makeRequestFirstPage")
      .mockResolvedValue(firstPageMockReturn);
    citizenSpyRequestAdditionalPages = jest
      .spyOn(GetCitizens.prototype, "makeRequestsAdditionalPages")
      .mockResolvedValue([]);

    const result = await searchOrchestration({
      params: mockParams,
      headers: mockHeaders,
    });

    expect(otherCertificatesSpy).toBeCalledTimes(1);
    expect(otherCertificatesSpy).toBeCalledWith(mockParams);
    expect(citizenSpyRequestFirstPage).toBeCalledTimes(1);
    expect(citizenSpyRequestFirstPage).toHaveBeenCalledWith(
      mockParams,
      mockOutgoingCallHeaders,
    );
    expect(citizenSpyRequestAdditionalPages).toBeCalledTimes(1);
    expect(citizenSpyRequestAdditionalPages).toHaveBeenCalledWith(
      mockParams,
      mockOutgoingCallHeaders,
      firstPageMockReturn.page?.totalPages || 0,
    );
    expect(certificateSpyRequestFirstPage).toBeCalledTimes(0);
    expect(certificateSpyRequestAdditionalPages).toBeCalledTimes(0);
    expect(result).toStrictEqual({
      exemptions: {
        "hrt-ppcs": [],
        others: [],
      },
    });
  });

  it("should call certificate service with additional filter parameters, no 'other' certificates", async () => {
    const firstPageMockReturn: CitizenDao = {
      _embedded: { citizens: [] },
      page: citizenApiResponseOnePage.page,
    };
    citizenSpyRequestFirstPage = jest
      .spyOn(GetCitizens.prototype, "makeRequestFirstPage")
      .mockResolvedValue(firstPageMockReturn);
    citizenSpyRequestAdditionalPages = jest
      .spyOn(GetCitizens.prototype, "makeRequestsAdditionalPages")
      .mockResolvedValue([]);

    const result = await searchOrchestration({
      params: mockParamsWithCertificateFilters,
      headers: mockHeaders,
    });

    expect(otherCertificatesSpy).toBeCalledTimes(1);
    expect(otherCertificatesSpy).toBeCalledWith(
      mockParamsWithCertificateFilters,
    );
    expect(citizenSpyRequestFirstPage).toBeCalledTimes(1);
    expect(citizenSpyRequestFirstPage).toHaveBeenCalledWith(
      mockParamsWithCertificateFilters,
      mockOutgoingCallHeaders,
    );
    expect(citizenSpyRequestAdditionalPages).toBeCalledTimes(1);
    expect(citizenSpyRequestAdditionalPages).toHaveBeenCalledWith(
      mockParamsWithCertificateFilters,
      mockOutgoingCallHeaders,
      firstPageMockReturn.page?.totalPages || 0,
    );
    expect(certificateSpyRequestFirstPage).toBeCalledTimes(0);
    expect(certificateSpyRequestAdditionalPages).toBeCalledTimes(0);
    expect(result).toStrictEqual({
      exemptions: {
        "hrt-ppcs": [],
        others: [],
      },
    });
  });

  it("should not make call to get other certificate types when firstName missing", async () => {
    const params = (({ firstName, ...o }) => o)(mockParams); // eslint-disable-line @typescript-eslint/no-unused-vars
    const firstPageMockReturn: CitizenDao = {
      _embedded: { citizens: [] },
      page: citizenApiResponseOnePage.page,
    };
    citizenSpyRequestFirstPage = jest
      .spyOn(GetCitizens.prototype, "makeRequestFirstPage")
      .mockResolvedValue(firstPageMockReturn);
    citizenSpyRequestAdditionalPages = jest
      .spyOn(GetCitizens.prototype, "makeRequestsAdditionalPages")
      .mockResolvedValue([]);

    const result = await searchOrchestration({
      params: params,
      headers: mockHeaders,
    });

    expect(otherCertificatesSpy).toBeCalledTimes(0);
    expect(result).toStrictEqual({
      exemptions: {
        "hrt-ppcs": [],
        others: [],
      },
    });
  });

  it("should not make call to get other certificate types when lastName missing", async () => {
    const params = (({ lastName, ...o }) => o)(mockParams); // eslint-disable-line @typescript-eslint/no-unused-vars
    const firstPageMockReturn: CitizenDao = {
      _embedded: {
        citizens: [],
      },
      page: citizenApiResponseOnePage.page,
    };
    citizenSpyRequestFirstPage = jest
      .spyOn(GetCitizens.prototype, "makeRequestFirstPage")
      .mockResolvedValue(firstPageMockReturn);
    citizenSpyRequestAdditionalPages = jest
      .spyOn(GetCitizens.prototype, "makeRequestsAdditionalPages")
      .mockResolvedValue([]);

    const result = await searchOrchestration({
      params: params,
      headers: mockHeaders,
    });

    expect(otherCertificatesSpy).toBeCalledTimes(0);
    expect(result).toStrictEqual({
      exemptions: {
        "hrt-ppcs": [],
        others: [],
      },
    });
  });

  it("should not make call to get other certificate types when dateOfBirth missing", async () => {
    const params = (({ dateOfBirth, ...o }) => o)(mockParams); // eslint-disable-line @typescript-eslint/no-unused-vars
    const firstPageMockReturn: CitizenDao = {
      _embedded: { citizens: [] },
      page: citizenApiResponseOnePage.page,
    };
    citizenSpyRequestFirstPage = jest
      .spyOn(GetCitizens.prototype, "makeRequestFirstPage")
      .mockResolvedValue(firstPageMockReturn);
    citizenSpyRequestAdditionalPages = jest
      .spyOn(GetCitizens.prototype, "makeRequestsAdditionalPages")
      .mockResolvedValue([]);

    const result = await searchOrchestration({
      params: params,
      headers: mockHeaders,
    });

    expect(otherCertificatesSpy).toBeCalledTimes(0);
    expect(result).toStrictEqual({
      exemptions: {
        "hrt-ppcs": [],
        others: [],
      },
    });
  });

  it("should not make call to get other certificate types when postcode missing", async () => {
    const params = (({ postcode, ...o }) => o)(mockParams); // eslint-disable-line @typescript-eslint/no-unused-vars
    const firstPageMockReturn: CitizenDao = {
      _embedded: { citizens: [] },
      page: citizenApiResponseOnePage.page,
    };
    citizenSpyRequestFirstPage = jest
      .spyOn(GetCitizens.prototype, "makeRequestFirstPage")
      .mockResolvedValue(firstPageMockReturn);
    citizenSpyRequestAdditionalPages = jest
      .spyOn(GetCitizens.prototype, "makeRequestsAdditionalPages")
      .mockResolvedValue([]);

    const result = await searchOrchestration({
      params: params,
      headers: mockHeaders,
    });

    expect(otherCertificatesSpy).toBeCalledTimes(0);
    expect(result).toStrictEqual({
      exemptions: {
        "hrt-ppcs": [],
        others: [],
      },
    });
  });
});

function citizenIdsDelimited(citizens: Array<Citizen>): string {
  const ids: Array<string> = [];
  for (const i in citizens) {
    ids.push(citizens[i].id);
  }
  return ids.join(",");
}
