import { GetCertificates } from "../get-certificates";
import {
  mockParams,
  mockParamsWithPageOne,
  mockParamsWithPageTwo,
  mockHeaders,
} from "../__mocks__/mockData";
import certificateApiResponseMultiPageFirst from "../__mocks__/api-responses/certificate-api/multi-page-results-page-1.json";
import certificateApiResponseMultiPageSecond from "../__mocks__/api-responses/certificate-api/multi-page-results-page-2.json";
import certificateApiResponseMultiPageThird from "../__mocks__/api-responses/certificate-api/multi-page-results-page-3.json";
import certificateApiResponseNoCertificates from "../__mocks__/api-responses/certificate-api/no-certificates-page-results.json";
import { CertificateApi } from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { CertificateDao } from "../interfaces";
let certificateSpy: jest.SpyInstance;
const getCertificates = new GetCertificates();

afterEach(() => {
  jest.resetAllMocks();
});

describe("get certificates", () => {
  it("should make request and respond with the first page results", async () => {
    certificateSpy = jest
      .spyOn(CertificateApi.prototype, "makeRequest")
      .mockResolvedValue(certificateApiResponseMultiPageFirst);

    const result = await getCertificates.makeRequestFirstPage(
      mockParams,
      mockHeaders,
    );

    const expectCertificateApiCalled = {
      method: "GET",
      url: "/v1/certificates/search/findByCitizens",
      params: mockParams,
      headers: mockHeaders,
      responseType: "json",
    };
    const expectedResult = {
      _embedded: {
        certificates:
          certificateApiResponseMultiPageFirst._embedded.certificates,
      },
      _links: {
        first: {
          href: "http://localhost:8090/v1/certificates/search/findByCitizens?citizenIds=c0a8012f-852e-13ca-8185-356ab044009c&page=0&size=20",
        },
        last: {
          href: "http://localhost:8090/v1/certificates/search/findByCitizens?citizenIds=c0a8012f-852e-13ca-8185-356ab044009c&page=2&size=20",
        },
        next: {
          href: "http://localhost:8090/v1/certificates/search/findByCitizens?citizenIds=c0a8012f-852e-13ca-8185-356ab044009c&page=1&size=20",
        },
        self: {
          href: "http://localhost:8090/v1/certificates/search/findByCitizens?citizenIds=c0a8012f-852e-13ca-8185-356ab044009c&page=0&size=20",
        },
      },
      page: certificateApiResponseMultiPageFirst.page,
    };

    expect(certificateSpy).toHaveBeenCalledWith(expectCertificateApiCalled);
    expect(result).toStrictEqual(expectedResult);
  });

  it("should make request and respond with the empty when no certificates found ", async () => {
    certificateSpy = jest
      .spyOn(CertificateApi.prototype, "makeRequest")
      .mockResolvedValue(certificateApiResponseNoCertificates);

    const result = await getCertificates.makeRequestFirstPage(
      mockParams,
      mockHeaders,
    );

    const expectCertificateApiCalled = {
      method: "GET",
      url: "/v1/certificates/search/findByCitizens",
      params: mockParams,
      headers: mockHeaders,
      responseType: "json",
    };
    const expectedResult = {
      _embedded: {
        certificates:
          certificateApiResponseNoCertificates._embedded.certificates,
      },
      _links: {
        self: {
          href: "http://localhost:8090/v1/certificates/search/findByCitizens?citizenIds=c0a8012f-852e-13ca-8185-356ab044009c&page=0&size=20",
        },
      },
      page: certificateApiResponseNoCertificates.page,
    };

    expect(certificateSpy).toHaveBeenCalledWith(expectCertificateApiCalled);
    expect(result).toStrictEqual(expectedResult);
  });

  it("should make request and respond with empty certificates array and no page object when results undefined", async () => {
    certificateSpy = jest
      .spyOn(CertificateApi.prototype, "makeRequest")
      .mockResolvedValue(undefined);

    const result = await getCertificates.makeRequestFirstPage(
      mockParams,
      mockHeaders,
    );

    const expectCertificateApiCalled = {
      method: "GET",
      url: "/v1/certificates/search/findByCitizens",
      params: mockParams,
      headers: mockHeaders,
      responseType: "json",
    };
    const expectedResult: CertificateDao = {
      _embedded: {
        certificates:
          certificateApiResponseNoCertificates._embedded.certificates,
      },
    };

    expect(certificateSpy).toHaveBeenCalledWith(expectCertificateApiCalled);
    expect(result).toStrictEqual(expectedResult);
  });

  it("should make request and respond with the second and third page results", async () => {
    certificateSpy = jest
      .spyOn(CertificateApi.prototype, "makeRequest")
      .mockResolvedValueOnce(certificateApiResponseMultiPageSecond)
      .mockResolvedValueOnce(certificateApiResponseMultiPageThird);
    const firstPage = certificateApiResponseMultiPageFirst.page;

    const result = await getCertificates.makeRequestsAdditionalPages(
      mockParamsWithPageOne,
      mockHeaders,
      firstPage.totalPages,
    );

    const expectCertificateApiCalledFirst = {
      method: "GET",
      url: "/v1/certificates/search/findByCitizens",
      params: mockParamsWithPageOne,
      headers: mockHeaders,
      responseType: "json",
    };
    const expectCertificateApiCalledSecond = {
      ...expectCertificateApiCalledFirst,
      params: mockParamsWithPageTwo,
    };
    const expectedResult =
      certificateApiResponseMultiPageSecond._embedded.certificates.concat(
        certificateApiResponseMultiPageThird._embedded.certificates,
      );

    expect(certificateSpy).toHaveBeenCalledTimes(2);
    expect(certificateSpy).toHaveBeenNthCalledWith(
      1,
      expectCertificateApiCalledFirst,
    );
    expect(certificateSpy).toHaveBeenNthCalledWith(
      2,
      expectCertificateApiCalledSecond,
    );
    expect(result).toStrictEqual(expectedResult);
  });

  it("should make request and respond with empty array when results undefined for additional pages", async () => {
    certificateSpy = jest
      .spyOn(CertificateApi.prototype, "makeRequest")
      .mockResolvedValue({ page: 1, _embedded: { certificates: [] } });
    const firstPage = certificateApiResponseMultiPageFirst.page;

    const result = await getCertificates.makeRequestsAdditionalPages(
      mockParamsWithPageOne,
      mockHeaders,
      firstPage.totalPages,
    );

    expect(certificateSpy).toHaveBeenCalledTimes(2);
    expect(result).toStrictEqual([]);
  });
});
