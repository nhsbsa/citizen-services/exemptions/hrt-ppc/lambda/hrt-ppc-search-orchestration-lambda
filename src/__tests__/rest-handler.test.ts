import { handler } from "../index";

import { searchOrchestration } from "../hrt-ppc-search-orchestration";
import successResponse from "../__mocks__/single-page-response.json";

import mockEvent from "../../events/event-all-fields.json";
import { APIGatewayEvent } from "aws-lambda";

jest.mock("../hrt-ppc-search-orchestration");

let event: APIGatewayEvent;
const mockSearch = searchOrchestration as jest.MockedFunction<
  typeof searchOrchestration
>;

beforeEach(() => {
  jest.useFakeTimers().setSystemTime(new Date("2020-01-01"));
  event = JSON.parse(JSON.stringify(mockEvent));
  mockSearch.mockClear();
  mockSearch.mockResolvedValue(successResponse);
});

describe("handler", () => {
  it("should respond with an bad request error if req body has no citizen", async () => {
    event.body = JSON.stringify({});
    const result = await handler(event);
    expect(result).toMatchInlineSnapshot(`
      {
        "body": "{"status":400,"message":"There were validation issues with the request","timestamp":"2020-01-01T00:00:00.000Z","fieldErrors":[{"field":"citizen.firstName, citizen.lastName, citizen.dateOfBirth, citizen.address.postcode","message":"At least one search parameter must not be null or empty"}]}",
        "statusCode": 400,
      }
    `);
  });

  it("should respond with an bad request error if request citizen fields are empty", async () => {
    event.body = JSON.stringify({
      firstName: "",
      lastName: "",
      dateOfBirth: "",
      address: {
        postcode: "",
      },
    });
    const result = await handler(event);
    expect(result).toMatchInlineSnapshot(`
      {
        "body": "{"status":400,"message":"There were validation issues with the request","timestamp":"2020-01-01T00:00:00.000Z","fieldErrors":[{"field":"citizen.firstName, citizen.lastName, citizen.dateOfBirth, citizen.address.postcode","message":"At least one search parameter must not be null or empty"}]}",
        "statusCode": 400,
      }
    `);
  });

  it("should respond with an bad request error if event body has no citizen", async () => {
    event.body = JSON.stringify({});
    const result = await handler(event);
    expect(result).toMatchInlineSnapshot(`
      {
        "body": "{"status":400,"message":"There were validation issues with the request","timestamp":"2020-01-01T00:00:00.000Z","fieldErrors":[{"field":"citizen.firstName, citizen.lastName, citizen.dateOfBirth, citizen.address.postcode","message":"At least one search parameter must not be null or empty"}]}",
        "statusCode": 400,
      }
    `);
  });

  it("should respond with an internal server error", async () => {
    mockSearch.mockRejectedValueOnce(Error("Internal Server Error"));

    const result = await handler(event);
    expect(result).toMatchInlineSnapshot(`
      {
        "body": "{"status":500,"message":"Internal Server Error","timestamp":"2020-01-01T00:00:00.000Z"}",
        "statusCode": 500,
      }
    `);
  });

  it("should respond with the 200 success response when citizens found", async () => {
    const result = await handler(event);
    expect(result).toEqual({
      statusCode: 200,
      body: JSON.stringify(successResponse),
    });
  });
});
