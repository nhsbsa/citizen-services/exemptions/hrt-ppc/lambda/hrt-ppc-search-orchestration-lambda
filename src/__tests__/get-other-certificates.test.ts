import { GetOtherCertificates } from "../get-other-certificates";
import { mockParams } from "../__mocks__/mockData";
import exemptionServiceNoMatch from "../__mocks__/api-responses/exemption-service-api/no-certificates-page-results.json";
import exemptionServiceSingleCertificate from "../__mocks__/api-responses/exemption-service-api/single-certificate-found.json";
import exemptionServiceMultipleCertificate from "../__mocks__/api-responses/exemption-service-api/multiple-certificates-found.json";
import expectedSingleCertificateResponse from "../__mocks__/expected-results/get-other-certificates/single-certificate-found.json";
import expectedMultipleCertificateResponse from "../__mocks__/expected-results/get-other-certificates/multiple-certificates-found.json";
import { ExemptionServiceApi } from "@nhsbsa/health-charge-exemption-npm-utils-rest";
let exemptionServiceSpy: jest.SpyInstance;
const getOtherCertificates = new GetOtherCertificates();

const expectedRequestBody = {
  dateOfBirth: new Date(mockParams.dateOfBirth),
  familyName: mockParams.lastName,
  firstName: mockParams.firstName,
  postcode: mockParams.postcode,
};

afterEach(() => {
  jest.resetAllMocks();
});

describe("get other certificates", () => {
  it("should make request and respond with the results when single certificate found", async () => {
    exemptionServiceSpy = jest
      .spyOn(ExemptionServiceApi.prototype, "makeRequest")
      .mockResolvedValue(exemptionServiceSingleCertificate);

    const result =
      await getOtherCertificates.makeRequestSearchByPersonalDetails(mockParams);

    const expectCertificateApiCalled = {
      method: "POST",
      url: "/v3/search/HRT_PPC/search-by-personal-details",
      data: expectedRequestBody,
      responseType: "json",
    };
    const expectedResult = {
      certificates: expectedSingleCertificateResponse,
    };

    expect(exemptionServiceSpy).toHaveBeenCalledWith(
      expectCertificateApiCalled,
    );
    expect(result).toStrictEqual(expectedResult);
  });

  it("should make request and respond with the results when multiple certificates found", async () => {
    exemptionServiceSpy = jest
      .spyOn(ExemptionServiceApi.prototype, "makeRequest")
      .mockResolvedValue(exemptionServiceMultipleCertificate);

    const result =
      await getOtherCertificates.makeRequestSearchByPersonalDetails(mockParams);

    const expectCertificateApiCalled = {
      method: "POST",
      url: "/v3/search/HRT_PPC/search-by-personal-details",
      data: expectedRequestBody,
      responseType: "json",
    };
    const expectedResult = {
      certificates: expectedMultipleCertificateResponse,
    };

    expect(exemptionServiceSpy).toHaveBeenCalledWith(
      expectCertificateApiCalled,
    );
    expect(result).toStrictEqual(expectedResult);
  });

  it("should make request and respond with empty when no certificates found", async () => {
    exemptionServiceSpy = jest
      .spyOn(ExemptionServiceApi.prototype, "makeRequest")
      .mockResolvedValue(exemptionServiceNoMatch);

    const result =
      await getOtherCertificates.makeRequestSearchByPersonalDetails(mockParams);

    const expectCertificateApiCalled = {
      method: "POST",
      url: "/v3/search/HRT_PPC/search-by-personal-details",
      data: expectedRequestBody,
      responseType: "json",
    };
    const expectedResult = {
      certificates: [],
    };

    expect(exemptionServiceSpy).toHaveBeenCalledWith(
      expectCertificateApiCalled,
    );
    expect(result).toStrictEqual(expectedResult);
  });

  it("should make request and respond with empty certificates array when results undefined", async () => {
    exemptionServiceSpy = jest
      .spyOn(ExemptionServiceApi.prototype, "makeRequest")
      .mockResolvedValue(undefined);

    const result =
      await getOtherCertificates.makeRequestSearchByPersonalDetails(mockParams);

    const expectCertificateApiCalled = {
      method: "POST",
      url: "/v3/search/HRT_PPC/search-by-personal-details",
      data: expectedRequestBody,
      responseType: "json",
    };
    const expectedResult = {
      certificates: [],
    };

    expect(exemptionServiceSpy).toHaveBeenCalledWith(
      expectCertificateApiCalled,
    );
    expect(result).toStrictEqual(expectedResult);
  });
});
