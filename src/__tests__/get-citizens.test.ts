import { GetCitizens } from "../get-citizens";
import {
  mockParams,
  mockParamsWithPageOne,
  mockHeaders,
} from "../__mocks__/mockData";
import citizenApiResponseMultiPageFirst from "../__mocks__/api-responses/citizen-api/multi-page-results-page-1.json";
import citizenApiResponseMultiPageSecond from "../__mocks__/api-responses/citizen-api/multi-page-results-page-2.json";
import citizenApiResponseMultiPageThird from "../__mocks__/api-responses/citizen-api/multi-page-results-page-3.json";
import citizenApiResponseNoCitizens from "../__mocks__/api-responses/citizen-api/no-citizens-page-results.json";
import { CitizenApi } from "@nhsbsa/health-charge-exemption-npm-utils-rest";
let citizenSpy: jest.SpyInstance;
const getCitizens = new GetCitizens();

afterEach(() => {
  jest.resetAllMocks();
});

describe("get citizens", () => {
  it("should make request and respond with the first page results", async () => {
    citizenSpy = jest
      .spyOn(CitizenApi.prototype, "makeRequest")
      .mockResolvedValue(citizenApiResponseMultiPageFirst);

    const result = await getCitizens.makeRequestFirstPage(
      mockParams,
      mockHeaders,
    );

    const expectCitizenApiCalled = {
      method: "GET",
      url: "/v1/citizens?firstName=John&lastName=Smith&dateOfBirth=1997-03-24&addresses.postcode=NE15+8NY",
      headers: mockHeaders,
      responseType: "json",
    };
    const expectedResult = {
      _embedded: {
        citizens: citizenApiResponseMultiPageFirst._embedded.citizens,
      },
      _links: {
        first: {
          href: "http://localhost:8100/v1/citizens?firstName=John&lastName=Smith&dateOfBirth=1997-03-24&postcode=NE158NY&page=0&size=2",
        },
        last: {
          href: "http://localhost:8100/v1/citizens?firstName=John&lastName=Smith&dateOfBirth=1997-03-24&postcode=NE158NY&page=2&size=2",
        },
        next: {
          href: "http://localhost:8100/v1/citizens?firstName=John&lastName=Smith&dateOfBirth=1997-03-24&postcode=NE158NY&page=1&size=2",
        },
        profile: {
          href: "http://localhost:8100/v1/profile/citizens",
        },
        search: {
          href: "http://localhost:8100/v1/citizens/search",
        },
        self: {
          href: "http://localhost:8100/v1/citizens?firstName=John&lastName=Smith&dateOfBirth=1997-03-24&postcode=NE158NY&size=2&page=0",
        },
      },
      page: citizenApiResponseMultiPageFirst.page,
    };

    expect(citizenSpy).toHaveBeenCalledWith(expectCitizenApiCalled);
    expect(result).toStrictEqual(expectedResult);
  });

  it("should make request and respond with the empty when no citizens found ", async () => {
    citizenSpy = jest
      .spyOn(CitizenApi.prototype, "makeRequest")
      .mockResolvedValue(citizenApiResponseNoCitizens);

    const result = await getCitizens.makeRequestFirstPage(
      mockParams,
      mockHeaders,
    );

    const expectCitizenApiCalled = {
      method: "GET",
      url: "/v1/citizens?firstName=John&lastName=Smith&dateOfBirth=1997-03-24&addresses.postcode=NE15+8NY",
      headers: mockHeaders,
      responseType: "json",
    };
    const expectedResult = {
      _embedded: {
        citizens: citizenApiResponseNoCitizens._embedded.citizens,
      },
      _links: {
        profile: {
          href: "http://localhost:8100/v1/profile/citizens",
        },
        search: {
          href: "http://localhost:8100/v1/citizens/search",
        },
        self: {
          href: "http://localhost:8100/v1/citizens?firstName=John&lastName=Smith&dateOfBirth=1998-03-24&postcode=NE158NY",
        },
      },
      page: citizenApiResponseNoCitizens.page,
    };

    expect(citizenSpy).toHaveBeenCalledWith(expectCitizenApiCalled);
    expect(result).toStrictEqual(expectedResult);
  });

  it("should make request and respond with empty citizens array and no page object when results undefined", async () => {
    citizenSpy = jest
      .spyOn(CitizenApi.prototype, "makeRequest")
      .mockResolvedValue(undefined);

    const result = await getCitizens.makeRequestFirstPage(
      mockParams,
      mockHeaders,
    );

    const expectCitizenApiCalled = {
      method: "GET",
      url: "/v1/citizens?firstName=John&lastName=Smith&dateOfBirth=1997-03-24&addresses.postcode=NE15+8NY",
      headers: mockHeaders,
      responseType: "json",
    };
    const expectedResult = {
      _embedded: {
        citizens: citizenApiResponseNoCitizens._embedded.citizens,
      },
    };

    expect(citizenSpy).toHaveBeenCalledWith(expectCitizenApiCalled);
    expect(result).toStrictEqual(expectedResult);
  });

  it("should make request and respond with the second and third page results", async () => {
    citizenSpy = jest
      .spyOn(CitizenApi.prototype, "makeRequest")
      .mockResolvedValueOnce(citizenApiResponseMultiPageSecond)
      .mockResolvedValueOnce(citizenApiResponseMultiPageThird);
    const firstPage = citizenApiResponseMultiPageFirst.page;

    const result = await getCitizens.makeRequestsAdditionalPages(
      mockParamsWithPageOne,
      mockHeaders,
      firstPage.totalPages,
    );

    const expectCitizenApiCalledFirst = {
      method: "GET",
      url: "/v1/citizens?page=1&firstName=John&lastName=Smith&dateOfBirth=1997-03-24&addresses.postcode=NE15+8NY",
      headers: mockHeaders,
      responseType: "json",
    };
    const expectCitizenApiCalledSecond = {
      ...expectCitizenApiCalledFirst,
      url: "/v1/citizens?page=2&firstName=John&lastName=Smith&dateOfBirth=1997-03-24&addresses.postcode=NE15+8NY",
    };
    const expectedResult =
      citizenApiResponseMultiPageSecond._embedded.citizens.concat(
        citizenApiResponseMultiPageThird._embedded.citizens,
      );

    expect(citizenSpy).toHaveBeenCalledTimes(2);
    expect(citizenSpy).toHaveBeenNthCalledWith(1, expectCitizenApiCalledFirst);
    expect(citizenSpy).toHaveBeenNthCalledWith(2, expectCitizenApiCalledSecond);
    expect(result).toStrictEqual(expectedResult);
  });

  it("should make request and respond with empty array when results undefined for additional pages", async () => {
    citizenSpy = jest
      .spyOn(CitizenApi.prototype, "makeRequest")
      .mockResolvedValue({ page: 1, _embedded: { citizens: [] } });
    const firstPage = citizenApiResponseMultiPageFirst.page;

    const result = await getCitizens.makeRequestsAdditionalPages(
      mockParamsWithPageOne,
      mockHeaders,
      firstPage.totalPages,
    );

    expect(citizenSpy).toHaveBeenCalledTimes(2);
    expect(result).toStrictEqual([]);
  });
});
