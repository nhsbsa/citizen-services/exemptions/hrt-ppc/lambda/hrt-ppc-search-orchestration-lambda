import { isNil } from "ramda";
import { loggerWithContext } from "@nhsbsa/hrt-ppc-npm-logging";
import { ExemptionServiceApi } from "@nhsbsa/health-charge-exemption-npm-utils-rest";

/**
 * The following class allows the retrieval of other exemptions.
 */
export class GetOtherCertificates {
  exemptionApi: ExemptionServiceApi;
  constructor() {
    this.exemptionApi = new ExemptionServiceApi();
  }

  public async makeRequestSearchByPersonalDetails(params) {
    const request = {
      dateOfBirth: new Date(params.dateOfBirth),
      postcode: params.postcode,
      familyName: params.lastName,
      firstName: params.firstName,
    };
    let { exemptions } = (await this.exemptionApi.makeRequest({
      method: "POST",
      url: "/v3/search/HRT_PPC/search-by-personal-details",
      data: request,
      responseType: "json",
    })) || { exemptions: [] };

    if (isNil(exemptions)) {
      exemptions = [];
    }

    loggerWithContext().info(
      `found [${exemptions.length}] number of exemption records for other types`,
    );

    const exemptionsResponse = exemptions.map(
      ({ certNumber, exemptionType, startDate, expiryDate }) => ({
        reference: certNumber.toString(),
        type: exemptionType,
        startDate: this.convertDateFromIso(new Date(startDate)),
        endDate: this.convertDateFromIso(new Date(expiryDate)),
        status: "ACTIVE",
      }),
    );

    return {
      certificates: exemptionsResponse,
    };
  }

  private convertDateFromIso(dateTime: Date): string {
    return dateTime.toISOString().substring(0, 10);
  }
}
