import { GetCitizens } from "./get-citizens";
import { GetCertificates } from "./get-certificates";
import { GetOtherCertificates } from "./get-other-certificates";
import { isNil } from "ramda";

import { RawAxiosRequestHeaders } from "axios";
import { transformHeadersAndReturnMandatory } from "@nhsbsa/health-charge-exemption-npm-utils-rest";
import { sliceArrayIntoChunks } from "./chunk-request";
import { SearchParams, SearchParamsCertificate } from "./types";
import { Channel } from "@nhsbsa/health-charge-exemption-npm-utils-rest/dist/src/clients/common";
import { loggerWithContext } from "@nhsbsa/hrt-ppc-npm-logging";
import {
  Certificate,
  CertificateDao,
  CitizenDao,
  Response,
} from "./interfaces";

const PAGE_SIZE = 500;

export async function searchOrchestration({
  params,
  headers,
}: {
  params: SearchParams;
  headers: RawAxiosRequestHeaders;
}) {
  params.size = PAGE_SIZE;
  loggerWithContext().info("Entered search orchestration service");

  const callingServiceHeaders = transformHeadersAndReturnMandatory(headers);

  const { citizensTranspose, citizensResponse } = await getCitizens(
    params,
    callingServiceHeaders,
  );

  const citizenIds: Array<string> = citizensTranspose.map(({ id }) => id);

  const certificatesResponse = await getCertificates(
    params,
    callingServiceHeaders,
    citizenIds,
  );

  if (certificatesResponse) {
    const otherExemptionsResponse = await getOtherExemptions(params);
    return buildResponse(
      otherExemptionsResponse,
      citizensResponse,
      certificatesResponse,
    );
  }

  const otherExemptionsResponse = await getOtherExemptions(params);
  return buildResponse(otherExemptionsResponse, citizensResponse, []);
}

async function getCertificates(
  params: SearchParams,
  callingServiceHeaders: {
    "user-id": string;
    channel: Channel;
    "correlation-id": string;
  },
  citizenIds: Array<string>,
) {
  const getCertificates = new GetCertificates();
  const certificatesTranspose: Certificate[] = [];
  if (citizenIds.length > 0) {
    // Since we make a GET call the uri length is limited, therefore we chunk the citizen ids
    const chunkedCitizenIds: string[][] = sliceArrayIntoChunks(citizenIds, 150);
    for (let i = 0; i < chunkedCitizenIds.length; i++) {
      loggerWithContext().info(
        `Get certificates chunk [${i + 1}] of total [${
          chunkedCitizenIds.length
        }]`,
      );
      const searchParamsCertificate: SearchParamsCertificate = {
        citizenIds: chunkedCitizenIds[i].join(","),
        endDate: params.endDate,
        startDate: params.startDate,
        reference: params.reference,
        status: params.status,
        size: PAGE_SIZE,
      };
      // first page call to get the certificates and page info
      const firstPageCertificateDao: CertificateDao =
        await getCertificates.makeRequestFirstPage(
          searchParamsCertificate,
          callingServiceHeaders,
        );

      // additional page calls to collate the data from other pages
      const certificatesAdditionalPages =
        await getCertificates.makeRequestsAdditionalPages(
          searchParamsCertificate,
          callingServiceHeaders,
          firstPageCertificateDao.page?.totalPages || 0,
        );

      // add both to an array for later processing
      const certificatesResults: Certificate[] = [
        ...firstPageCertificateDao._embedded.certificates,
        ...certificatesAdditionalPages,
      ];
      certificatesTranspose.push(...certificatesResults);
    }

    const certificatesResponse = certificatesTranspose.map(
      ({ citizenId, id, reference, type, status, startDate, endDate }) => ({
        id,
        citizenId,
        reference,
        type,
        status,
        startDate,
        endDate,
      }),
    );

    loggerWithContext().info(
      `total of [${certificatesResponse.length}] certificates found`,
    );
    return certificatesResponse;
  }
}

async function getCitizens(
  params: SearchParams,
  callingServiceHeaders: {
    "user-id": string;
    channel: Channel;
    "correlation-id": string;
  },
) {
  const getCitizens = new GetCitizens();

  // first page call to get the citizens and page info
  const firstCitizenPageDao: CitizenDao =
    await getCitizens.makeRequestFirstPage(params, callingServiceHeaders);

  // additional page calls to collate the data from other pages
  const citizensAdditionalPages = await getCitizens.makeRequestsAdditionalPages(
    params,
    callingServiceHeaders,
    firstCitizenPageDao.page?.totalPages || 0,
  );

  // add both to an array for later processing
  const citizensTranspose = [
    ...firstCitizenPageDao._embedded.citizens,
    ...citizensAdditionalPages,
  ];

  const citizensResponse = citizensTranspose.map(
    ({ id, firstName, lastName, dateOfBirth, addresses }) => ({
      id,
      firstName,
      lastName,
      dateOfBirth,
      addresses: addresses.map(
        ({
          id,
          type,
          addressLine1,
          addressLine2,
          townOrCity,
          country,
          postcode,
        }) => ({
          id,
          addressLine1,
          addressLine2,
          townOrCity,
          country,
          postcode,
          type,
        }),
      ),
    }),
  );

  loggerWithContext().info(
    `total of [${citizensResponse.length}] citizens found across [${firstCitizenPageDao.page?.totalPages}] pages`,
  );
  return { citizensTranspose, citizensResponse };
}

async function getOtherExemptions(params) {
  // Exemption service requires all params so only call if these are provided in incoming request
  if (
    !isNil(params.firstName) &&
    !isNil(params.lastName) &&
    !isNil(params.dateOfBirth) &&
    !isNil(params.postcode)
  ) {
    const getOtherCertificates = new GetOtherCertificates();

    const { certificates: exemptions } =
      await getOtherCertificates.makeRequestSearchByPersonalDetails(params);

    const exemptionsResponse = {
      citizens: [
        {
          firstName: params.firstName,
          lastName: params.lastName,
          dateOfBirth: params.dateOfBirth,
          certificates: exemptions,
        },
      ],
    };

    return exemptionsResponse;
  }
  loggerWithContext().info(
    "Skipping call to exemption service as required params not provided",
  );
}

function buildResponse(
  otherExemptionsResponse,
  citizensResponse,
  certificatesResponse?,
) {
  const response: Response = { exemptions: {} };
  if (citizensResponse.length === 0) {
    response.exemptions["hrt-ppcs"] = [];
  } else {
    citizensResponse.map((citizen) => {
      // filter out only the certificates for this citizen
      const filteredCertificatesForCitizen = certificatesResponse.filter(
        (cert) => cert.citizenId === citizen.id,
      );
      // the citizenId is not to be added to response
      filteredCertificatesForCitizen.forEach(
        (certificate) => delete certificate.citizenId,
      );
      citizen.certificates = filteredCertificatesForCitizen;
    });
    response.exemptions["hrt-ppcs"] = [{ citizens: citizensResponse }];
  }
  if (
    isNil(otherExemptionsResponse) ||
    otherExemptionsResponse.citizens[0].certificates.length === 0
  ) {
    response.exemptions["others"] = [];
  } else {
    response.exemptions["others"] = [otherExemptionsResponse];
  }

  return response;
}
