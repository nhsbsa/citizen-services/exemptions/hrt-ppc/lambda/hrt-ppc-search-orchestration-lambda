import { AxiosRequestConfig } from "axios";

/**
 * This function try to cut an array into chunks.
 * @param arr input array
 * @param chunkSize chunk size
 * Return array of chunk array.
 */
export function sliceArrayIntoChunks<
  T extends number | string | AxiosRequestConfig,
>(arr: T[], chunkSize: number): T[][] {
  if (chunkSize < 1) throw new Error("chunk size cannot less than 1");
  const chunks: T[][] = [];
  let i = 0;
  const n = arr.length;
  while (i < n) {
    chunks.push(arr.slice(i, (i += chunkSize)));
  }
  return chunks;
}
