export interface Meta {
  channel: string;
  createdTimestamp: string;
  createdBy: string;
  updatedTimestamp: string;
  updatedBy: string;
}

export interface Links {
  citizen?: Link;
  profile?: Link;
  self?: Link;
  certificate?: Link;
  first?: Link;
  prev?: Link;
  next?: Link;
}

export interface Link {
  href: string;
}

export interface Address {
  id: string;
  type: string;
  addressLine1: string;
  addressLine2?: string;
  townOrCity: string;
  country?: string;
  postcode: string;
  _meta: Meta;
  _links: Links;
}

export interface EmailAddress {
  id: string;
  type: string;
  emailAddress: string;
  _meta: Meta;
  _links: Links;
}

export interface Page {
  size: number;
  totalElements: number;
  totalPages: number;
  number: number;
}

export interface Citizen {
  id: string;
  firstName: string;
  lastName: string;
  dateOfBirth: string;
  nhsNumber?: string;
  addresses: Address[];
  emails?: EmailAddress[];
  _meta: Meta;
  _links: Links;
}

export interface Citizens {
  citizens: Citizen[];
}

export interface CitizenDao {
  _embedded: Citizens;
  _links?: Links;
  page?: Page;
}

export interface Certificate {
  id: string;
  citizenId: string;
  reference: string;
  type: string;
  applicationDate: string;
  duration: string;
  startDate: string;
  endDate: string;
  cost: number;
  status: string;
  pharmacyId?: string;
  _meta: Meta;
  _links: Links;
}

export interface Certificates {
  certificates: Certificate[];
}

export interface CertificateDao {
  _embedded: Certificates;
  _links?: Links;
  page?: Page;
}

export interface Exemptions {
  "hrt-ppcs"?: unknown;
  others?: unknown;
}

export interface Response {
  exemptions: Exemptions;
}
